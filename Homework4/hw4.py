import os
import sys
from operator import itemgetter
from utils import *

ind = Indexer()
ind.initialize()

sentences = ind.get_texts()

while True:
    query = input('\nEnter a search query:')
    search = ind.filter_and_normalize_words(query.split(" "))

    found = ind.search(search)
    if not found:
        print('No relevant documents found')
        continue
    result = sorted(found.items(), key=itemgetter(1))[:5]
    it = 0
    for doc in result:
        it+=1
        print (it, ' ', sentences[int(doc[0])])
    user_choice = input('\nEnter a list of relevant docs:')
    ind.update_relevance(search, user_choice.strip().split(" "))



