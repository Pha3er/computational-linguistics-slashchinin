from index_utils import *

ind = Indexer()
ind.initialize()
search_index = ind.create_index()
print("Stop words:")
print(ind.stop_words)
print("----------------------------------------------------------")
while True:
    print("What do you want to find? Enter your search query: ")
    too_long = False
    try:
        query = input()
    except:
        continue
    if len(query) > 255:
        print("Your search query is too long, try again.")
        continue
    words = ind.filter_and_normalize_words(query.split(" "))
    found = {}
    for word in words:
        match = search_index.get(word)
        if match:
            if not found:
                found = set(match)
            else:
                found = found.intersection(match)
        else:
            found = {-50}
    if  found == {-50}:
        print("No matching texts found")
        continue

    print("Found in ", len(found), " texts:")
    texts = ind.get_texts()
    for i in found:
        try:
            print(texts[i])
        except:
            texts[i] = texts[i][1:]
            print(texts[i])
