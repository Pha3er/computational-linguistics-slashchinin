# -*- coding: utf-8-sig -*-
import nltk
import codecs
import string
import sys
import os
import json
#nltk.download()
import math
from nltk.corpus import stopwords
from collections import Counter
from lxml import etree
import pymorphy2

class Indexer(): 

    texts = []
    stop_words = []
    morph = None
    index = {}
    def initialize(self):
        self.morph = pymorphy2.MorphAnalyzer()
        print("Initialization. Please wait...")
        self.stop_words = stopwords.words('russian')
        self.stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в', ' '])
        self.stop_words.extend(string.punctuation)
        print (os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), os.pardir, 'Classwork1', 'Economics&Finance.xml'))
        self.parse_xml(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), os.pardir, 'Classwork1', 'Economics&Finance.xml'))
        self.parse_xml(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), os.pardir,'Classwork1', 'Travelling&Tourism.xml'))
        if os.path.exists(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json')):
            print ('Loading from json file')
            index_file = open(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json'), 'r')
            self.index = json.load(index_file)
            index_file.close()
        else:
            print ('Creating new index')
            self.create_index()

    def parse_xml(self, filePath):
        parser = etree.XMLParser(ns_clean=True, encoding='utf8')
        tree = etree.parse(filePath, parser)
        root = tree.getroot()
        for sentences in root:
            for sentence in sentences:
                self.texts.append(sentence.text)

    def filter_and_normalize_words(self, words):
        return list(map(lambda word: self.morph.parse(word)[0].normal_form,
                        filter(lambda word: word not in self.stop_words, words)))

    def create_index(self):

        text_index = 0;
        documents_with_idf = self.idf_docs()
        for doc in documents_with_idf:
            for word, idf in doc.items():
                if self.index.get(word):
                    self.index.get(word).update({text_index: idf})
                else:
                    self.index.update({word: {text_index: idf} })
            text_index +=1
        index_file = open(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json'), 'w')
        json.dump(self.index, index_file, sort_keys=True, indent=4)

    def make_normlized_list(self):
        _stdout = sys.stdout
        null = open(os.devnull,'wb')
        sys.stdout = null
        normalized_words = []
        for text in self.texts:
            try:
                sys.stdout.write(text[0])
            except:           
                text = text[1:]
            normalized_words.append(self.filter_and_normalize_words(text.split(' ')))        
        sys.stdout = _stdout
        return normalized_words

    def idf_docs(self):
        norm_list = self.make_normlized_list()

        def compute_idf(word, words_list):
            return math.log10(len(words_list)/sum([1.0 for i in words_list if word in i]))

        documents_list = []
        for words in norm_list:
            idf_dictionary = {}
            for word in words:
                idf_dictionary[word] = compute_idf(word, norm_list)
            documents_list.append(idf_dictionary)
        return documents_list

    def get_texts(self):
        return self.texts

    def get_index(self):
        return self.index

    def search(self, query):
        docs = set()
        for word in query: 
            value = self.index.get(word)
            if (value):
                for sent_ind in value:
                    docs.add(sent_ind)

        relevant_docs = {}
        for doc in docs:
            doc_sum = 0
            for word in query:
                if self.index.get(word) and self.index.get(word).get(doc):
                    doc_sum += self.index.get(word).get(doc)
            relevant_docs.update({doc: doc_sum})

        return relevant_docs
    
    