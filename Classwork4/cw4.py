import os
import sys
from utils import *

ind = Indexer()
ind.initialize()

sentences = ind.get_texts()

while True:
    query = input('\nEnter a search query:')
    search = ind.filter_and_normalize_words(query.split(" "))

    found = ind.search(search)
    if not found:
        print('No relevant documents found')
        continue

    sort = sorted(found.items())
    result = sort[:10]
    for doc in result:
        print(sentences[doc[0]])



