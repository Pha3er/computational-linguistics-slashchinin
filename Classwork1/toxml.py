# -*- coding: utf8 -*-
import os
import re
import codecs
from lxml import etree

def directory_to_xml(directory):
    files = os.listdir(directory)
    page = etree.Element('Documents')

    for file in files:
        headElt = etree.SubElement(page, 'Document')
        s = codecs.open(os.path.join(directory, file), "r", "utf-8").read()
        sentences = filter(lambda x: x, re.split(r"[\.!?\n\r]", s))
        for sentence in sentences:
            st = re.sub(r"[$«»–()\.,%/—\-:;\n]", '', sentence)
            if st == '':
                continue
            title = etree.SubElement(headElt, 'sentence')
            title.text = st
            with codecs.open(directory + '.xml', 'w', "utf-8") as output:
                output.write(etree.tounicode(page))

directory1 = 'Economics&Finance'
directory2 = 'Travelling&Tourism'
directory_to_xml(directory1)
directory_to_xml(directory2)



