from lxml import etree
import codecs
from treetaggerwrapper import TreeTagger

def parse_xml(filePath):
    texts = []
    parser = etree.XMLParser(ns_clean=True, encoding='utf8')
    tree = etree.parse(filePath, parser)
    root = tree.getroot()
    for sentences in root:
        for sentence in sentences:
            texts.append(sentence.text)
    return texts

f = codecs.open('tagged_text.txt', 'w', 'utf8')
texts = parse_xml('../Classwork1/Economics&Finance.xml')
tt = TreeTagger(TAGLANG='ru',TAGDIR='C:/TreeTagger')
text_tags = tt.TagText(texts[0])
tagged_text = ""
for word in text_tags:
    tagged_text += word + " "
f.write(tagged_text)