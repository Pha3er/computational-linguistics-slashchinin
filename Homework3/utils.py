# -*- coding: utf-8-sig -*-
import nltk
import codecs
import string
import sys
import os
import json
#nltk.download()
import math
from nltk.corpus import stopwords
from collections import Counter
from lxml import etree
import pymorphy2

class Indexer(): 

    texts = []
    stop_words = []
    morph = None
    index = {}
    def initialize(self):
        self.morph = pymorphy2.MorphAnalyzer()
        print("Initialization. Please wait...")
        self.stop_words = stopwords.words('russian')
        self.stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в', ' '])
        self.stop_words.extend(string.punctuation)
        print (os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), os.pardir, 'Classwork1', 'Economics&Finance.xml'))
        self.parse_xml(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), os.pardir, 'Classwork1', 'Economics&Finance.xml'))
        self.parse_xml(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), os.pardir,'Classwork1', 'Travelling&Tourism.xml'))
        if os.path.exists(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json')):
            print ('Loading from json file')
            index_file = open(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json'), 'r')
            self.index = json.load(index_file)
            index_file.close()
        else:
            print ('Creating new index')
            self.create_index()

    def parse_xml(self, filePath):
        parser = etree.XMLParser(ns_clean=True, encoding='utf8')
        tree = etree.parse(filePath, parser)
        root = tree.getroot()
        for sentences in root:
            for sentence in sentences:
                self.texts.append(sentence.text)

    def filter_and_normalize_words(self, words):
        return list(map(lambda word: self.morph.parse(word)[0].normal_form,
                        filter(lambda word: word not in self.stop_words, words)))

    def create_index(self):

        text_index = 0;
        documents_list = self.compute_tfidf()
        for doc in documents_list:
            for word in doc:
                if self.index.get(word):
                    self.index.get(word).append(text_index)
                else:
                    self.index.update({word: [text_index]})
            text_index +=1
        index_file = open(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json'), 'w')
        json.dump(self.index, index_file, sort_keys=True, indent=4)

    def make_normlized_list(self):
        _stdout = sys.stdout
        null = open(os.devnull,'wb')
        sys.stdout = null
        normalized_words = []
        for text in self.texts:
            try:
                sys.stdout.write(text[0])
            except:           
                text = text[1:]
            normalized_words.append(self.filter_and_normalize_words(text.split(' ')))        
        sys.stdout = _stdout
        return normalized_words



    def get_texts(self):
        return self.texts

    def get_index(self):
        return self.index
    
    def compute_tfidf(self):
        words_list = self.make_normlized_list()
        def compute_tf(text):
            tf_text = Counter(text)
            for i in tf_text:
                tf_text[i] = tf_text[i]/float(len(tf_text))
            return tf_text

        def compute_idf(word, words_list):
            return math.log10(len(words_list)/sum([1.0 for i in words_list if word in i]))

        documents_list = []
        for text in words_list:
            tf_idf_dictionary = {}
            computed_tf = compute_tf(text)
            for word in computed_tf:
                tf_idf_dictionary[word] = computed_tf[word] * compute_idf(word, words_list)
            documents_list.append(tf_idf_dictionary)
        return documents_list