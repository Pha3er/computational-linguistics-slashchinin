import math
import codecs
import collections
from lxml import etree
import pymorphy2

morph = pymorphy2.MorphAnalyzer()

def parse_xml(filePath):
    texts = []
    parser = etree.XMLParser(ns_clean=True, encoding='utf8')
    tree = etree.parse(filePath, parser)
    root = tree.getroot()
    for sentences in root:
        for sentence in sentences:
            texts.append(sentence.text)
    return texts

def normalize(word):
    global morph
    normalized = morph.parse(word)[0].normal_form
    return normalized

onegram_map = dict()
onegrams = codecs.open('../1grams-3.txt', 'r', 'utf8') #onegrams from OpenCorpora
total_one = 0

min_one = 10

for line in onegrams.readlines():
    s = line.split('\t')
    onegram_map[s[1]] = float(s[0])
    total_one += float(s[0])

for key in onegram_map:
    onegram_map[key] /= total_one
    if min_one > onegram_map[key]:
        min_one = onegram_map[key]


texts = parse_xml('../Classwork1/Economics&Finance.xml')

total_sum = 0
M = 0 #number of words in test sample
for sent in texts:
    sent_sum = 0
    s = sent.split()
    M += len(s)
    if len(s) < 1:
        continue
    for i in range(len(s)):
        sent_sum +=  math.log(onegram_map[s[i]]) if onegram_map.get(s[i], -1) > 0 else math.log(min_one)
    
    total_sum += sent_sum

perplexity = 2**(-(1/M)*total_sum)
print("Total perplexity:")
print (perplexity)



