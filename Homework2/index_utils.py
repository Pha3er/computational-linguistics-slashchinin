# -*- coding: utf-8-sig -*-
import nltk
import codecs
import string
import sys
import os
#nltk.download()
from nltk.corpus import stopwords
from lxml import etree
import pymorphy2
class Indexer():    
    texts = []
    stop_words = []
    morph = None
    def initialize(self):
        self.morph = pymorphy2.MorphAnalyzer()
        print("Initialization. Please wait...")
        self.stop_words = stopwords.words('russian')
        self.stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в', ' '])
        self.stop_words.extend(string.punctuation)
        self.parse_xml('Economics&Finance.xml')
        self.parse_xml('Travelling&Tourism.xml')

    def parse_xml(self, filePath):
        parser = etree.XMLParser(ns_clean=True, encoding='utf8')
        tree = etree.parse(filePath, parser)
        root = tree.getroot()
        for sentences in root:
            for sentence in sentences:
                self.texts.append(sentence.text)

    def filter_and_normalize_words(self, words):
        return list(map(lambda word: self.morph.parse(word)[0].normal_form,
                        filter(lambda word: word not in self.stop_words, words)))

    def create_index(self):
        index = {}
        _stdout = sys.stdout
        null = open(os.devnull,'wb')
        sys.stdout = null
        text_index = 0;
        for text in self.texts:
            try:
                sys.stdout.write(text[0])
            except:           
                text = text[1:]
            words = self.filter_and_normalize_words(nltk.word_tokenize(text))
            for word in words:
                if index.get(word):
                    index.get(word).append(text_index)
                else:
                    index.update({word: [text_index]})
            text_index +=1
        sys.stdout = _stdout
        return index
    def get_texts(self):
        return self.texts
    