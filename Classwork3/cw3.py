import os
import sys
from utils import *
from scipy import spatial

ind = Indexer()
ind.initialize()
search_index = ind.get_index()

sentences = ind.get_texts()

while True:
    query = input('\nEnter a search query:')
    search = ind.filter_and_normalize_words(query.split(" "))

    docs = set()  
    search_binary_vector = []
    for word in search:  # for all words in query
        # build a search vector
        if search_index.get(word):
            search_binary_vector.append(1)
        else:
            search_binary_vector.append(0)

        value = search_index.get(word)
        if (value):
            for sent_ind in value:
                docs.add(sent_ind)
        else:
            print('No relevant documents found')
            continue

    docs_binary_vectors = {}
    for doc in docs:
        doc_vector = []
        for word in search:
            if doc in search_index.get(word):
                doc_vector.append(1)
            else:
                doc_vector.append(0)
        docs_binary_vectors.update({doc: doc_vector})

    cosine_vector = {}
    for doc_id, doc_vector in docs_binary_vectors.items():
        cosine_vector.update({doc_id: spatial.distance.cosine(doc_vector, search_binary_vector)})
    sort = sorted(cosine_vector.items())
    result = sort[:5]
    for doc in result:
        print(sentences[doc[0]])



