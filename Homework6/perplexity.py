import math
import codecs
import collections
from lxml import etree
import pymorphy2

morph = pymorphy2.MorphAnalyzer()

def parse_xml(filePath):
    texts = []
    parser = etree.XMLParser(ns_clean=True, encoding='utf8')
    tree = etree.parse(filePath, parser)
    root = tree.getroot()
    for sentences in root:
        for sentence in sentences:
            texts.append(sentence.text)
    return texts

def normalize(word):
    global morph
    normalized = morph.parse(word)[0].normal_form
    return normalized

onegram_map = dict()
bigram_map = collections.defaultdict(lambda: collections.defaultdict(float))
onegrams = codecs.open('../1grams-3.txt', 'r', 'utf8') #onegrams from OpenCorpora
bigrams =  codecs.open('../2grams-3.txt', 'r', 'utf8') #bigrams from OpenCorpora
total_one = 0
total_bi = 0

min_one = 10
min_bi = 10

for line in onegrams.readlines():
    s = line.split('\t')
    onegram_map[s[1]] = float(s[0])
    total_one += float(s[0])

for key in onegram_map:
    onegram_map[key] /= total_one
    if min_one > onegram_map[key]:
        min_one = onegram_map[key]

for line in bigrams.readlines():
    s = line.split('\t')
    bigram_map[s[1]][s[3]] = float(s[0])
    total_bi += float(s[0])

for k1, d in bigram_map.items():
    for k2 in d:
        bigram_map[k1][k2] /= total_bi
        if min_bi > bigram_map[k1][k2]:
            min_bi = bigram_map[k1][k2]


texts = parse_xml('../Classwork1/Economics&Finance.xml')

total_sum = 0
M = 0 #number of words in test sample
for sent in texts:
    sent_sum = 0
    s = sent.split()
    M += len(s)
    if len(s) < 1:
        continue
    sent_sum += math.log(onegram_map[s[0]]) if onegram_map.get(s[0], -1) > 0 else math.log(min_one)
    for i in range(1, len(s)):
        sent_sum += math.log(bigram_map[s[i-1]][s[i]]) if bigram_map.get(s[i-1], {}).get(s[i], -1) > 0 else math.log(min_bi)
    total_sum += sent_sum

perplexity = 2**(-(1/M)*total_sum)
print("Total perplexity:")
print (perplexity)



